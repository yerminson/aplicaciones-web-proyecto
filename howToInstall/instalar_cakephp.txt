---- version actual de cakephp 2.1.2

*descargue el codigo fuente de cakephp
-puede hacerlo usando git ->  git clone git://github.com/cakephp/cakephp.git
-puede decargarlo de la pagina principal -> http://cakephp.org/

*copie la carpeta en /var/www y renombrela a cakephp

*edite el archivo /etc/apache2/sites-available/default para que tenga algo como
<Directory /var/www/>
		Options Indexes FollowSymLinks MultiViews
		AllowOverride All
#		Order allow,deny
#		allow from all
	</Directory>

*ejecute el siguiente comando
$ sudo a2enmod rewrite  -> debe de indicar que se a habilitado el modulo

-revise que en el archivo /etc/apache2/mods-enabled/rewrite.load se encuentre: LoadModule rewrite_module /usr/lib/apache2/modules/mod_rewrite.so

*cambie los permisos de /var/www/cakephp/app/tmp ejecutando:
$ sudo chmod -R 777 /var/www/cakephp/app/tmp

*reinicie apache ejecuntado :
$ sudo service apache2 restart

*en su navegador vaya a http://localhost/cakephp
-debera de ver en verde 
   -Your version of PHP is 5.2.8 or higher.
   -Your tmp directory is writable.
   -The FileEngine is being used for core caching. To change the config edit APP/Config/core.php

-en rojo dos lineas relacionadas con Security.salt y Security.cipherSeed

*para solucionar la lineas rojas edite el archivo /var/ww/cakephp/app/Config/core.php
-busque Configure::write('Security.salt', '...'); -> cambie ... por el string que desee
-busque Configure::write('Security.cipherSeed', '...'); -> cambie ... por el string(numero) que desee

*para configurar la base de datos(mongodb)
-en la carpeta /var/www/cakephp/app/Plugin ejecute
$ sudo git clone  git://github.com/ichikaway/cakephp-mongodb.git  Mongodb
-ingrese a la carpteta recien creada Mongodb y ejecute
$ sudo git checkout -b cake2.0 origin/cake2.0

-renombre el archivo /var/ww/cakephp/Config/database.php.default por database.php
-abra el archivo database.php, vera que tiene definida una clase llamada DATABASE_CONFIG, comente las dos definiciones de array que hay en esta
-agregue lo siguiente:
   public $default = array(
      'datasource' => 'Mongodb.MongodbSource',
      'host' => 'localhost',
      'database' => 'aplication',
      'port' => 27017
	);

-edite el archivo /var/www/cakephp/Config/bootstrap.php, al final de este agregue:
CakePlugin::load('Mongodb');

