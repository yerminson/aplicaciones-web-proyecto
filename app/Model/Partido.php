<?php
    class Partido extends AppModel
    {
        public $name = 'Partido';
        public $primaryKey = '_id';
        
        public $validate = array(
            'marcador1' => array(
                'required' => array(
                    'rule' => array('notEmpty'),
                    'message' => 'Debe ingreser el marcador del jugador o pareja 1'
                ),
                'numero' => array(
                    'rule' => array('numeric'),
                    'message' => 'El marcador 1 debe ser un numero entre 0 y 8.'
                )
            ),
            'marcador2' => array(
                'required' => array(
                    'rule' => array('notEmpty'),
                    'message' => 'Debe ingreser el marcador del jugador o pareja 2'
                ),
                'numero' => array(
                    'rule' => array('numeric'),
                    'message' => 'El marcador 2 debe ser un numero entre 0 y 8.'
                )
            )
        );
        
        
       /*public function beforeSave(){ 
            $marcador1=$this->data['Partido']['jugador1'];
            $marcador2=$this->data['Partido']['jugador2'];
            unset($this->data['Partido']['modalidad']);
            unset($this->data['Partido']['jugador1']);
            unset($this->data['Partido']['jugador2']);
            unset($this->data['Partido']['marcador1']);
            unset($this->data['Partido']['marcador2']); 
            $this->data['Partido']['estado'] = 'creado';
            $this->data['Partido']['jugador1']['_id']=$marcador1;
            $this->data['Partido']['jugador2']['_id']=$marcador2;            
            
            pr($this->data['Partido']);
            return true;
        }
        public function beforeSave(){ 
            if(isset($this->data['Partido']['marcador1'])){
                $marcador1=$this->data['Partido']['marcador1'];
                $marcador2=$this->data['Partido']['marcador2'];
                $jugador1=$this->data['Partido']['jugador1'];
                $jugador2=$this->data['Partido']['jugador2'];
                unset($this->data['Partido']['modalidad']);
                unset($this->data['Partido']['jugador1']);
                unset($this->data['Partido']['jugador2']);
                unset($this->data['Partido']['marcador1']);
                unset($this->data['Partido']['marcador2']); 
                $this->data['Partido']['jugador1']['_id']=$jugador1;
                $this->data['Partido']['jugador2']['_id']=$jugador2;
                $this->data['Partido']['jugador1']['marcador']=$marcador1;
                $this->data['Partido']['jugador2']['marcador']=$marcador2;
                if($marcador1>$marcador2){
                    $this->data['Partido']['jugador1']['gano']="true";
                    $this->data['Partido']['jugador2']['gano']="false";
                }else{
                    $this->data['Partido']['jugador1']['gano']="true";
                    $this->data['Partido']['jugador2']['gano']="false";
                }            
            }            
            pr($this->data['Partido']);
            return true;
        }*/
        
        /*
         * $mongoShema
         */
    }
?>
