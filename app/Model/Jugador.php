<?php

    class Jugador extends AppModel
    {
        public $name = 'Jugador';
        public $primaryKey = '_id';
        
        public $validate = array(
            'nombres' => array(
                'required' => array(
                    'rule' => array('notEmpty'),
                    'message' => 'Debe de proporcionar el nombre(s) del jugador'
                )
            ),
            'apellidos' => array(
                'required' => array(
                    'rule' => array('notEmpty'),
                    'message' => 'Debe de proporcionar el apellido(s) del jugador'
                )
            ),
            'identificacion' => array(
                'isUnique' => array(
                    'rule' => array('isUnique'),
                    'message' => 'Ya existe un jugador con esta identificacion',
                    'required' => 'create'
                ),
                'required' => array(
                    'rule' => array('notEmpty'),
                    'message' => 'Debe de proporcionar la identificacion del jugador'
                )
            )
        );
        /*
         * $mongoSchema = array (
         * 'nombres' => array('type' => string),
         * 'apellidos' => array('type' => string),
         * 'identificacion' => array('type' => string),
         * 'fecha_nacimiento' => array('type' => array(month => array('type' => string),
         *                                             day => array('type' => string),
         *                                             year => array('type' => string))),
         * 'sexo' => array('type' => string),
         * 'categoria' => array('type' => string),
         * 'ranking' => array('type' => number)
         * ) 
         */
    }
?>
