<?php

class GranTorneo extends AppModel {

    public $name = 'GranTorneo';
    public $primaryKey = '_id';
    var $validate = array(
        //Validacion duracion 
        'duracion' => array(
            'rule' => 'numeric', // ó: array('ruleName', 'param1', 'param2' ...)
            'required' => true,
            'allowEmpty' => false,
            'on' => 'create', // ó: 'update'
            'message' => 'Debe proporcionar el valor de la duracion del torneo, es un valor númerico.'
        ),
        //validacion costo inscripcion
        'costo_inscripcion' => array(
            'rule' => 'numeric', // ó: array('ruleName', 'param1', 'param2' ...)
            'required' => true,
            'allowEmpty' => false,
            'on' => 'create', // ó: 'update'
            'message' => 'Debe proporcionar el valor de la inscripcion al torneo, es un valor númerico.'
        ),
        //validacion nombre
        'nombre' => array(
            'notEmpy' => array(
                'rule' => 'notEmpty', // ó: array('ruleName', 'param1', 'param2' ...)
                'on' => 'create', // ó: 'update'
                'message' => 'Debe proporcionar un nombre para el torneo.'),
            'unique' => array(
                'rule' => 'isUnique',
                'on' => 'create',
                'message' => 'El nombre que trada de usar ya existe')
            ),
        //validacion canchas disponibles
        'canchas_disponibles' => array(
            'rule' => 'numeric', // ó: array('ruleName', 'param1', 'param2' ...)
            'required' => true,
            'allowEmpty' => false,
            'on' => 'create', // ó: 'update'
            'message' => 'Debe proporcionar el número de canchas para el torneo, es un valor numérico.'
        ),
        //validacion cantidad jugadores maxima
        'cantidad_maxima_jugadores' => array(
            'rule' => 'numeric', // ó: array('ruleName', 'param1', 'param2' ...)
            'required' => true,
            'allowEmpty' => false,
            'on' => 'create', // ó: 'update'
            'message' => 'Debe proporcionar la cantidad de jugadores máxima para el torneo, es un valor numérico.'
        )
    );

    /*
     * $mongoShema = array(
     * 'fecha_inicio' => array('type' => array(month => array('type' => string),
     *                                             day => array('type' => string),
     *                                             year => array('type' => string))),
     * duracion => array('type' => number),
     * costo_incripcion => array('type' => number),
     * premios => array(),
     * canchas_disponibles => array('type' => number),
     * cantidad_maxima_jugadores => array('type' => number),
     * nombre => array('type' => string)
     * )
     */ 
}

?>
