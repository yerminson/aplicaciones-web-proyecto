<?php
    App::uses('AuthComponent', 'Controller/Component');
    class User extends AppModel
    {
        public $name = 'User';
        public $primaryKey = '_id';
        
        public $validate = array(
           'nombres' => array(
                'required' => array(
                    'rule' => array('notEmpty'),
                    'message' => 'Debe proporcionar el nombre(s)'
                )
            ),
            'apellidos' => array(
                'required' => array(
                    'rule' => array('notEmpty'),
                    'message' => 'Debe proporcionar el apellido(s)'
                )
            ),
            'identificacion' => array(
                'isUnique' => array(
                    'rule' => array('isUnique'),
                    'message' => 'Ya existe un juez o administrador con esta identificacion'
                ),
                'required' => array(
                    'rule' => array('notEmpty'),
                    'message' => 'Debe proporcionar la identificación'
                )
            ),
	    'contrasena' => array(
            	'required' => array(
                    'rule'    => array('between', 5, 100),
                    'message' => 'Su contraseña debe tener al menos 5 caracteres'
                 ),
	        'Match passwords' => array(
                     'rule' => 'matchPasswords',
		     'message' => 'Las Contraseñas no coinciden'
	         )
             ),
	     'confirmar_contrasena' => array(
                'required' => array(
                     'rule' => array('notEmpty'),
                     'message' => 'Por favor confirme su contraseña'
                )
              ),
             'experiencia' => array(
		'allowEmpty' => array(
                    'required' => false,
                    'allowEmpty' => true,
                    'rule' => array('numeric'),
                    'message' => 'Debe ingresar un número'
                )
             )
        );

   /**
    * Metodo que permite verificar si las contraseñas coninciden. (Campos
    * contraseña y repetir_contraseña)
    * @return void
    * @access public
    */
    public function matchPasswords($data){
        if($data['contrasena'] == $this->data['User']['confirmar_contrasena']){
	   return true;
	}
	$this->invalidate('confirmar_contrasena', 'Las Contraseñas no coinciden');
	return false;
    }

   /**
    * Metodo beforeSave, se llama antes de guardar, es de cake fue reescrito para que encripte 
    * la contraseña.
    * @return void
    * @access public
    */
    public function beforeSave(){
        //Asegurarnos de que no guarde experiencia para administradores
	if($this->data['User']['tipo'] == 'Administrador'){
	    unset($this->data['User']['experiencia']);
	}
	if(isset($this->data['User']['contrasena'])){
	   $this->data['User']['contrasena'] = AuthComponent::password($this->data['User']['contrasena']);
	}
	unset($this->data['User']['confirmar_contrasena']);
	return true;
    }


        /*
         * $mongoSchema = array (
         * 'nombres' => array('type' => string),
         * 'apellidos' => array('type' => string),
         * 'fecha_nacimiento' => array('type' => array(month => array('type' => string),
         *                                             day => array('type' => string),
         *                                             year => array('type' => string)))
         * 'identificacion' => array('type' => string),
	 * 'contrasena' => array('type' => string),
	 * 'tipo' => array('type' => string),
	 * 'experiencia' => array('type' => string)
         * ) 
         */
    }
?>
