<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title><?php echo $title_for_layout; ?></title>
        <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css(array('reset', 'text', 'grid', 'layout', 'nav'));
        echo '<!--[if IE 6]>' . $this->Html->css('ie6') . '<![endif]-->';
        echo '<!--[if IE 7]>' . $this->Html->css('ie') . '<![endif]-->';
        echo $this->Html->script(array('jquery-1.3.2.min.js', 'jquery-ui.js', 'jquery-fluid16.js'));
        echo $scripts_for_layout;
        ?>
    </head>
    <body>
        <div class="container_16">	
            <div class="grid_16">
                <h1 id="branding">
                    <a href="">Gestión de Torneos de Tenis</a>
                </h1>
            </div>
            <div class="clear"></div>
            <div class="grid_16">
                <ul class="nav main">
                    <li><?php echo $this->Html->link('Inicio', array('controller' => 'pages', 'action' => 'home')); ?></li>
                    <li><a href="">Torneos</a></li>
                    <li><?php echo $this->Html->link('Rankings', array('controller' => 'jugadors', 'action' => 'rankings')); ?></li>
                    <li><?php echo $this->Html->link('Posiciones por Torneo', array('controller' => 'torneos', 'action' => 'verTablaPosiciones')); ?></li>
                    <li><a href="">Proximos Torneos</a></li>
                    <li  class="secondary" >
                        <?php if ($logged_in): ?>                             
                            <?php echo $this->Html->link('Cerrar Sesión', array('controller' => 'users', 'action' => 'logout')); ?>
                        <?php else: ?>
                            <?php echo $this->Html->link('Login', array('controller' => 'users', 'action' => 'login')); ?>
                        <?php endif; ?>
                    </li>                
                    <?php if ($logged_in): ?>
                        <?php echo "<li> <a href=#>Administración</a>"; ?>
                            <?php echo "<ul>"; ?>
                            <!-- Funcionalidades de Administrador -->
                            <?php if ($user['tipo'] == 'Administrador'): ?>                
                                <?php
                                echo "<li>" . $this->Html->link('Crear Torneo', array('controller' => 'granTorneos', 'action' => 'registrar')) . "</li>";
                                echo "<li>" . $this->Html->link('Registrar Jugadores', array('controller' => 'jugadors', 'action' => 'registrar')) . "</li>";
                                echo "<li>" . $this->Html->link('Registrar Parejas', array('controller' => 'parejas', 'action' => 'registrar')) . "</li>";
                                echo "<li>" . $this->Html->link('Conformar Grupos', array('controller' => 'torneos', 'action' => 'conformarGrupos'), array('confirm' => 'Este proceso puede eliminar categorias con menos de 4 jugadores. Desa continuar?')) . "</li>";
                                echo "<li>" . $this->Html->link('Programar Torneos', array('controller' => 'torneos', 'action' => 'realizarProgramacion')) . "</li>";
                                echo "<li>" . $this->Html->link('Registrar Usuarios', array('controller' => 'users', 'action' => 'registrar')) . "</li>";
                                ?>                                
                            <?php else: ?><!-- Funcionalidades de Juez -->
                                <?php echo "<li>" . $this->Html->link('Registrar Marcador', array('controller' => 'partidos', 'action' => 'registrarMarcador')) . "</li>"; ?>
                            <?php endif; ?>
                            <?php echo"</ul> </li>"; ?>
                        <?php echo "<li> <a href=#>Consultas</a>"; ?> 
                     <?php echo "<ul>"; ?>                            
                            <!-- Funcionalidades de Administrador y Juez -->
                            <?php
                            echo "<li>" . $this->Html->link('Consultar Jugadores', array('controller' => 'jugadors', 'action' => 'listar')) . "</li>";
                            echo "<li>" . $this->Html->link('Consultar Usuarios', array('controller' => 'users', 'action' => 'listar')) . "</li>";
                            echo "<li>" . $this->Html->link('Consultar Parejas', array('controller' => 'parejas', 'action' => 'listar')) . "</li>";
                            echo "<li>" . $this->Html->link('Consultar Torneos', array('controller' => 'granTorneos', 'action' => 'listar')) . "</li>";
                            echo "<li>" . $this->Html->link('Consultar Grupos', array('controller' => 'torneos', 'action' => 'verGrupos')) . "</li>";
                            echo "<li>" . $this->Html->link('Consultar Partidos', array('controller' => 'partidos', 'action' => 'listar')) . "</li>";
                        echo"</ul> </li>";
                        ?>
                    <?php endif; ?>
                </ul>
            </div>


            <div class="grid_16">
                <h2 id="page-heading">

                    <?php if ($logged_in): ?> 
                        Bienvenido(a), <?php echo $user['nombres']; ?> <?php echo $user['apellidos']; ?>
                    <?php endif; ?>
                </h2>      
            </div>
            <?php echo $this->Session->flash(); ?>
            <?php echo $content_for_layout; ?>
            <div class="clear"></div>
            <div class="clear"></div>
            <div class="grid_16" id="site_info">
                <div class="box">
                    <p>Gestión de Torneos de Tenis,  <a href="http://www.univalle.edu.co">Universidad del Valle</a>, interfaz basada en  <a href="http://960.gs/">960 Grid System</a> by <a href="http://sonspring.com/journal/960-grid-system">Nathan Smith</a>. Released under the 
                        <a href="#">GPL</a> / <a href="#">MIT</a> <a href="#">Licenses</a>.</p>
                </div>
            </div>
            <div class="clear"></div>       

            <div id="log">
                <?php echo $this->element('sql_dump'); ?>
            </div>
        </div>

    </body>

</html>
