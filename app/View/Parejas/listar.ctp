<?php echo $this->Html->script('jquery', FALSE) ?>
<div class="grid_16"> 
    <div class="block" id="tables" >

        <div class="box" id="listarParejas">
            <h2>
                <a href="#" id="">Listar Parejas</a>
            </h2>
            <br/>

            <?php
            echo $this->Form->create('Pareja');
            echo $this->Form->input('opcion', array('type' => 'select',
                'options' => array('1' => 'Identificación J1', '2' => 'Identificación J2', '3' => 'Nombres J1', '4' => 'Nombres J2'),
                'label' => 'Opción de Búsqueda'));
            echo $this->Form->input('valor', array('label' => 'Parámetro de Búsqueda', 'id' => 'valor'));
            echo $this->Form->end();

            echo $this->Js->get('#valor')->event('keyup', $this->Js->request(array('controller' => 'parejas', 'action' => 'listar'), array(//'update' => '#success',
                        'update' => '#listaParejas',
                        'method' => 'post',
                        'async' => true,
                        'dataExpression' => true,
                        'data' => $this->Js->serializeForm(array(
                            'isForm' => false,
                            'inline' => true
                        ))
                    )));
            ?>
            <br/><br/>
            <div id="listaParejas">
                <?php include 'lista.ctp'; ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Js->writeBuffer(); ?>
