
<?php
echo $this->Form->hidden('_id', array('value' => $id));
echo $this->Form->input('nombresJ1', array('label' => 'Nombres primer jugador', 'value' => $nombresJ1));
echo $this->Form->input('apellidosJ1', array('label' => 'Apellidos primer jugador', 'value' => $apellidosJ1));
echo $this->Form->input('fecha_nacimientoJ1', array('type' => 'date', 'dateFormat' => 'DMY', 'maxYear' => date('Y') - 18, 'minYear' => date('Y') - 100, 'label' => 'Fecha de Nacimiento primer jugador', 'selected' => $fecha_nacimientoJ1));
echo $this->Form->input('generoJ1', array('label' => 'Género primer jugador', 'type' => 'select', 'options' => array('Masculino' => 'Masculino', 'Femenino' => 'Femenino'), 'value' => $generoJ1));
echo $this->Form->input('nombresJ2', array('label' => 'Nombres segundo jugador', 'value' => $nombresJ2));
echo $this->Form->input('apellidosJ2', array('label' => 'Apellidos segundo jugador', 'value' => $apellidosJ2));
echo $this->Form->input('fecha_nacimientoJ2', array('type' => 'date', 'dateFormat' => 'DMY', 'maxYear' => date('Y') - 18, 'minYear' => date('Y') - 100, 'label' => 'Fecha de Nacimiento segundo jugador', 'selected' => $fecha_nacimientoJ2));
echo $this->Form->input('generoJ2', array('label' => 'Género segundo jugador', 'type' => 'select', 'options' => array('Masculino' => 'Masculino', 'Femenino' => 'Femenino'), 'value' => $generoJ2));
echo $this->Form->input('categoria', array('type' => 'select', 'label' => 'Selecciona la categoría', 'options' => $categorias, 'value' => $categoria));
?>
