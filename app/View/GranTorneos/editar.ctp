<div class="grid_16">   
    <div class="block">  

        <div class="box" id="login-box">
            <h2>
                <a href="#" id="toggle-login-forms">Editar datos del Torneo</a>
            </h2>


            <div class="form-granTorneo" id="form-granTorneo">
                <?php
                echo $this->Form->create('GranTorneo', array('action' => 'editar', 'type' => 'post'));
                if ($this->request->data['GranTorneo']['estado'] == 'creado') {
                    echo $this->Form->input('fecha_inicio', array('type' => 'date', 'maxYear' => date('Y') + 5, 'minYear' => date('Y'), 'label' => 'Fecha de inicio'));
                    echo $this->Form->input('duracion', array('div' => 'input'));
                    echo $this->Form->input('canchas_disponibles', array('div' => 'input'));
                    echo $this->Form->input('cantidad_maxima_jugadores', array('div' => 'input'));
                    echo $this->Form->input('costo_inscripcion', array('div' => 'input'));
                }
                echo $this->Form->input('nombre', array('div' => 'input'));
                ?>

            </div>
            <fieldset>
                <legend>Premios</legend>
                <div id="accordion">
                    <h3 class="toggler atStart ui-accordion-header ui-helper-reset ui-state-active ui-corner-top" role="tab" aria-expanded="true" tabindex="0" style="font-weight: bold; background-color: rgb(255, 255, 255); background-position: initial initial; background-repeat: initial initial; "><span class="ui-icon ui-icon-triangle-1-s"></span>Premios categoria 1</h3>
                    <div>
                        <fieldset>
                            <?php
                            echo $this->Form->hidden('GranTorneo.premios.0.categoria', array('value' => '1'));
                            echo $this->Form->hidden('GranTorneo.premios.0.puesto', array('value' => 1));
                            echo 'Puesto 1';
                            echo $this->Form->input('GranTorneo.premios.0.valor');

                            echo $this->Form->hidden('GranTorneo.premios.1.categoria', array('value' => '1'));
                            echo $this->Form->hidden('GranTorneo.premios.1.puesto', array('value' => 2));
                            echo 'Puesto 2';
                            echo $this->Form->input('GranTorneo.premios.1.valor');

                            echo $this->Form->hidden('GranTorneo.premios.2.categoria', array('value' => '1'));
                            echo $this->Form->hidden('GranTorneo.premios.2.puesto', array('value' => 3));
                            echo 'Puesto 3';
                            echo $this->Form->input('GranTorneo.premios.2.valor');
                            ?>
                        </fieldset>
                    </div>
                    <h3 class="toggler atStart ui-accordion-header ui-helper-reset ui-state-active ui-corner-top" role="tab" aria-expanded="true" tabindex="0" style="font-weight: bold; background-color: rgb(255, 255, 255); background-position: initial initial; background-repeat: initial initial; "><span class="ui-icon ui-icon-triangle-1-s"></span>Premios categoria 2</h3>
                    <div>
                        <fieldset>
                            <?php
                            echo $this->Form->hidden('GranTorneo.premios.3.categoria', array('value' => '2'));
                            echo $this->Form->hidden('GranTorneo.premios.3.puesto', array('value' => 1));
                            echo 'Puesto 1';
                            echo $this->Form->input('GranTorneo.premios.3.valor');

                            echo $this->Form->hidden('GranTorneo.premios.4.categoria', array('value' => '2'));
                            echo $this->Form->hidden('GranTorneo.premios.4.puesto', array('value' => 2));
                            echo 'Puesto 2';
                            echo $this->Form->input('GranTorneo.premios.4.valor');

                            echo $this->Form->hidden('GranTorneo.premios.5.categoria', array('value' => '2'));
                            echo $this->Form->hidden('GranTorneo.premios.5.puesto', array('value' => 3));
                            echo 'Puesto 3';
                            echo $this->Form->input('GranTorneo.premios.5.valor');
                            ?>
                        </fieldset>
                    </div>
                    <h3 class="toggler atStart ui-accordion-header ui-helper-reset ui-state-active ui-corner-top" role="tab" aria-expanded="true" tabindex="0" style="font-weight: bold; background-color: rgb(255, 255, 255); background-position: initial initial; background-repeat: initial initial; "><span class="ui-icon ui-icon-triangle-1-s"></span>Premios categoria 3</h3>
                    <div>
                        <fieldset>           
                            <?php
                            echo $this->Form->hidden('GranTorneo.premios.6.categoria', array('value' => '3'));
                            echo $this->Form->hidden('GranTorneo.premios.6.puesto', array('value' => 1));
                            echo 'Puesto 1';
                            echo $this->Form->input('GranTorneo.premios.6.valor');

                            echo $this->Form->hidden('GranTorneo.premios.7.categoria', array('value' => '3'));
                            echo $this->Form->hidden('GranTorneo.premios.7.puesto', array('value' => 2));
                            echo 'Puesto 2';
                            echo $this->Form->input('GranTorneo.premios.7.valor');

                            echo $this->Form->hidden('GranTorneo.premios.8.categoria', array('value' => '3'));
                            echo $this->Form->hidden('GranTorneo.premios.8.puesto', array('value' => 3));
                            echo 'Puesto 3';
                            echo $this->Form->input('GranTorneo.premios.8.valor');
                            ?>
                        </fieldset>
                    </div>
                    <h3 class="toggler atStart ui-accordion-header ui-helper-reset ui-state-active ui-corner-top" role="tab" aria-expanded="true" tabindex="0" style="font-weight: bold; background-color: rgb(255, 255, 255); background-position: initial initial; background-repeat: initial initial; "><span class="ui-icon ui-icon-triangle-1-s"></span>Premios categoria 4</h3>
                    <div>
                        <fieldset>

                            <?php
                            echo $this->Form->hidden('GranTorneo.premios.9.categoria', array('value' => '4'));
                            echo $this->Form->hidden('GranTorneo.premios.9.puesto', array('value' => 1));
                            echo 'Puesto 1';
                            echo $this->Form->input('GranTorneo.premios.9.valor');

                            echo $this->Form->hidden('GranTorneo.premios.10.categoria', array('value' => '4'));
                            echo $this->Form->hidden('GranTorneo.premios.10.puesto', array('value' => 2));
                            echo 'Puesto 2';
                            echo $this->Form->input('GranTorneo.premios.10.valor');

                            echo $this->Form->hidden('GranTorneo.premios.11.categoria', array('value' => '4'));
                            echo $this->Form->hidden('GranTorneo.premios.11.puesto', array('value' => 3));
                            echo 'Puesto 3';
                            echo $this->Form->input('GranTorneo.premios.11.valor');
                            ?>
                        </fieldset>
                    </div>
                    <h3 class="toggler atStart ui-accordion-header ui-helper-reset ui-state-active ui-corner-top" role="tab" aria-expanded="true" tabindex="0" style="font-weight: bold; background-color: rgb(255, 255, 255); background-position: initial initial; background-repeat: initial initial; "><span class="ui-icon ui-icon-triangle-1-s"></span>Premios categoria Novatos</h3>
                    <div>
                        <fieldset>             
                            <?php
                            echo $this->Form->hidden('GranTorneo.premios.12.categoria', array('value' => 'Novatos'));
                            echo $this->Form->hidden('GranTorneo.premios.12.puesto', array('value' => 1));
                            echo 'Puesto 1';
                            echo $this->Form->input('GranTorneo.premios.12.valor');

                            echo $this->Form->hidden('GranTorneo.premios.13.categoria', array('value' => 'Novatos'));
                            echo $this->Form->hidden('GranTorneo.premios.13.puesto', array('value' => 2));
                            echo 'Puesto 2';
                            echo $this->Form->input('GranTorneo.premios.13.valor');

                            echo $this->Form->hidden('GranTorneo.premios.14.categoria', array('value' => 'Novatos'));
                            echo $this->Form->hidden('GranTorneo.premios.14.puesto', array('value' => 3));
                            echo 'Puesto 3';
                            echo $this->Form->input('GranTorneo.premios.14.valor');
                            ?>
                        </fieldset>
                    </div>
                </div>
            </fieldset>
            <?php echo $this->Form->end('Actualizar'); ?>
            <br>

            <?php if ($this->request->data['GranTorneo']['estado'] == 'creado'): ?>
                <h2>Jugadores Inscritos al Torneo</h2>
                <table>
                    <caption>Participantes modalidad Solo</caption>
                    <thead>
                        <tr>
                            <th>Identificación</th>
                            <th>Nombres</th>
                            <th>Género</th>
                            <th>Categoria</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($jugadores as $jugador): ?>
                            <tr>
                                <td><?php echo $jugador['Jugador']['identificacion'] ?></td>
                                <td><?php echo $jugador['Jugador']['nombres'] ?></td>
                                <td><?php echo $jugador['Jugador']['genero'] ?></td>
                                <td><?php echo $jugador['Jugador']['categoria'] ?></td>
                                <td>
                                    <?php echo $this->Html->link('Detalle', array('controller' => 'jugadors', 'action' => 'detalle', $jugador['Jugador']['_id'])); ?> &nbsp;
                                    <?php
                                    echo $this->Html->link('Eliminar del Torneo', array('controller' => 'torneos', 'action' => 'quitarDeTorneo', $jugador['Jugador']['_id'], $this->request->data['GranTorneo']['_id']), array('confirm' => 'Realmente está seguro?'));
                                    ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <table>
                    <caption>Participantes modalidad Pareja</caption>
                    <thead>
                        <tr>
                            <th>Identificación J1</th>
                            <th>Nombres J1</th>
                            <th>Identificación J2</th>
                            <th>Nombres J2</th>
                            <th>Categoria</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($parejas as $pareja): ?>
                            <tr>
                                <td><?php echo $pareja['Pareja']['identificacionJ1'] ?></td>
                                <td><?php echo $pareja['Pareja']['nombresJ1'] ?></td>
                                <td><?php echo $pareja['Pareja']['identificacionJ2'] ?></td>
                                <td><?php echo $pareja['Pareja']['nombresJ2'] ?></td>
                                <td><?php echo $pareja['Pareja']['categoria'] ?></td>
                                <td>
                                    <?php echo $this->Html->link('Detalle', array('controller' => 'parejas', 'action' => 'detalle', $pareja['Pareja']['_id'])); ?> &nbsp;
                                    <?php
                                    echo $this->Html->link('Eliminar del Torneo', array('controller' => 'torneos', 'action' => 'quitarDeTorneo', $pareja['Pareja']['_id'], $this->request->data['GranTorneo']['_id']), array('confirm' => 'Realmente está seguro?'));
                                    ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>

