<?php echo $this->Html->script('jquery', FALSE) ?>
<div class="grid_16"> 
    <div class="block" >


        <div class="box" >
            <h2>
                <a href="#" id="">Registrar Marcador</a>
            </h2>
            <br/>

            <?php
            echo $this->Form->create('Partido', array('action' => 'registrarMarcador', 'type' => 'post'));
            echo $this->Form->input('gran_torneo', array('type' => 'select', 'options' => $nombreTorneos, 'label' => 'Seleccione el Torneo', 'id' => 'gran_torneo'));
            echo $this->Form->input('modalidad', array('type' => 'select', 'options' => array('Masculino'=>'Masculino','Femenino'=>'Femenino','Parejas'=>'Parejas'), 'label' => 'Seleccione la modalidad', 'id' => 'modalidad'));
            ?>

            <fieldset class="form-partido" >
                <div id="partido">
                    <?php include 'aux_registro_marcadores.ctp'; ?>
                </div>
            </fieldset>

            <div id="end">
                    <?php
                    if(!empty ($nombreTorneos)){
                        echo $this->Form->end('Registrar');
                    } 
                    ?>
            </div>

            <?php
            echo $this->Js->get('#gran_torneo')->event('change', 
                    $this->Js->request(array('controller' => 'partidos', 'action' => 'registrarMarcador'), array(//'update' => '#success',
                        'update' => '#partido',
                        'method' => 'post',
                        'async' => true,
                        'dataExpression' => true,
                        'data' => $this->Js->serializeForm(array(
                            'isForm' => false,
                            'inline' => true
                        ))
                    )));
            echo $this->Js->get('#modalidad')->event('change', 
                    $this->Js->request(array('controller' => 'partidos', 'action' => 'registrarMarcador'), array(//'update' => '#success',
                        'update' => '#partido',
                        'method' => 'post',
                        'async' => true,
                        'dataExpression' => true,
                        'data' => $this->Js->serializeForm(array(
                            'isForm' => false,
                            'inline' => true
                        ))
                    )));
            ?>
           
        </div>
    </div>
</div>
<?php echo $this->Js->writeBuffer(); ?>