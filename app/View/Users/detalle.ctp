<div class="grid_16"> 
    <div class="block" >
        <div class="box" id="users-box">


            <h2>Detalle de datos del Usuario</h2>
            <br/>
            <table>
                <tbody>
                    <tr>
                        <td>Identificación:</td>
                        <td><?php echo $usera['User']['identificacion'] ?></td>
                    </tr>
                    <tr>
                        <td>Nombres:</td>
                        <td><?php echo $usera['User']['nombres'] ?></td>
                    </tr>
                    <tr>
                        <td>Apellidos:</td>
                        <td><?php echo $usera['User']['apellidos'] ?></td>
                    </tr>
                    <tr>
                        <td>Fecha de Nacimiento:</td>
                        <td>Día: <?php echo $usera['User']['fecha_nacimiento']['day'] ?>
                            Mes: <?php echo $usera['User']['fecha_nacimiento']['month'] ?>
                            Año: <?php echo $usera['User']['fecha_nacimiento']['year'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Tipo de Usuario:</td>
                        <td><?php echo $usera['User']['tipo'] ?></td>
                    </tr>
                    <?php if ($usera['User']['tipo'] == 'Juez') : ?>
                        <tr>
                            <td>Experiencia:</td>
                            <td><?php echo $usera['User']['experiencia'] ?> Años</td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
            <?php
            ?>
        </div>
    </div>
</div>