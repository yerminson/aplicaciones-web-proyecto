<?php echo $this->Html->script('jquery', FALSE) ?>
<div class="grid_16"> 
    <div class="block" id="tables" >

        <div class="box" id="listarUsuarios">
            <h2>
                <a href="#" id="">Listar Usuarios</a>
            </h2>
            <br/>

            <?php
            echo $this->Form->create('User');
            echo $this->Form->input('opcion', array('type' => 'select',
                'options' => array('1' => 'Identificación', '2' => 'Nombres', '3' => 'Apellidos'),
                'label' => 'Opción de Búsqueda'));
            echo $this->Form->input('valor', array('label' => 'Parámetro de Búsqueda', 'id' => 'valor'));
            echo $this->Form->end();

            echo $this->Js->get('#valor')->event('keyup', $this->Js->request(array('controller' => 'users', 'action' => 'listar'), array(//'update' => '#success',
                        'update' => '#listaUsuarios',
                        'method' => 'post',
                        'async' => true,
                        'dataExpression' => true,
                        'data' => $this->Js->serializeForm(array(
                            'isForm' => false,
                            'inline' => true
                        ))
                    )));
            ?>
            <br/><br/>
            <div id="listaUsuarios">
                <?php include 'lista.ctp'; ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Js->writeBuffer(); ?>
