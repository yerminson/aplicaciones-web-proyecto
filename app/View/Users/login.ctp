<div class="grid_16">
    <div class="box" id="login-box">
        <h2>
            <a href="#" id="toggle-login-forms">Ingreso al sistema</a>
        </h2>
        <div class="block" id="login-forms">
            <?php echo $this->Session->flash('auth'); ?>
            <?php echo $this->Form->create('User'); ?>
            <fieldset class="login">
                <div id="login-image">
                    <?php echo $this->Html->image('login.png', array('alt' => 'cancha')); ?>
                </div>
                <!--Instrucción del formulario-->
                <legend> Login </legend>
                <?php
                //Campos de entrada
                echo $this->Form->input('identificacion', array('label' => 'Identificación', 'div' => 'input'));
                echo $this->Form->input('contrasena', array('type' => 'password', 'label' => 'Contraseña', 'div' => 'input'));
                ?>
                <!--Boton para guardar en base de datos-->
                <?php echo $this->Form->submit('Ingresar'); ?>	
            </fieldset>


        </div>

    </div>
</div>
