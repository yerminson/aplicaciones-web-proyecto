<div class="grid_16"> 
    <div class="block" >
        <div class="box" id="users-box">
            <h2>
                <a href="#" id="">Registrar Administrador o Juez</a>
            </h2>
            <br/>
            <fieldset class="form-users">
                <?php
                echo $this->Form->create('User', array('action' => 'registrar', 'type' => 'post'));
                echo $this->Form->input('identificacion', array('label' => 'Identificación'));
                echo $this->Form->input('nombres');
                echo $this->Form->input('apellidos');
                echo $this->Form->input('fecha_nacimiento', array('type' => 'date', 'maxYear' => date('Y') - 18, 'minYear' => date('Y') - 100, 'label' => 'Fecha de Nacimiento'));
                echo $this->Form->input('contrasena', array('type' => 'password', 'label' => 'Crea una Contraseña'));
                echo $this->Form->input('confirmar_contrasena', array('type' => 'password', 'label' => 'Confirma tu contraseña'));
                echo $this->Form->input('tipo', array('label' => 'Tipo de User', 'type' => 'select', 'options' => array('Administrador' => 'Administrador', 'Juez' => 'Juez')));
                ?>
                <legend>Solo para Jueces</legend>
                <?php
                echo $this->Form->input('experiencia', array('label' => 'Experiencia en años'));
                ?>
            </fieldset>
            <?php echo $this->Form->end('Registrar'); ?>
        </div>
    </div>
</div>



