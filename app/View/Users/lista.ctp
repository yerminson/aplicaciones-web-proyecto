<table>
    <thead>
        <tr>
            <th>Identificación</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Tipo</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user): ?>
            <tr>
                <td><?php echo $user['User']['identificacion'] ?></td>
                <td><?php echo $user['User']['nombres'] ?></td>
                <td><?php echo $user['User']['apellidos'] ?></td>
                <td><?php echo $user['User']['tipo'] ?></td>
                <td>
                    <?php echo $this->Html->link('Detalle', array('action' => 'detalle', $user['User']['_id'])) ?> &nbsp;
                    <?php echo $this->Html->link('Editar', array('action' => 'editar', $user['User']['_id'])) ?> &nbsp;
                    <?php
                        echo $this->Form->postLink(
                        'Eliminar', array('action' => 'Eliminar', $user['User']['_id']), array('confirm' => 'Realmente está seguro?'))
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
