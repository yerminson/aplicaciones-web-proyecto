<div class="grid_16"> 
    <div class="block" id="tables">  
        <div class="box" id="verGrupos">
             <?php if(count($nombreTorneos)>0): ?>
                <h2>
                    <a href="#" id="">Conformar Grupos de un Torneo</a>
                </h2>
                <br/>
                <?php
                    echo $this->Form->create('Torneo', array('action' => 'conformarGrupos'));
                    echo $this->Form->input('gran_torneo', array('type' => 'select', 'options' => $nombreTorneos, 'label' => 'Seleccione el Torneo', 'id' => 'gran_torneo'));
                    //echo $this->Html->link('Consultar estado categorias', array('controller' => 'torneos','action' => 'consultarCategoriasTorneo'));
                ?>
                <div id="end"><?php echo $this->Form->end('Conformar',array('controller' => 'torneos', 'action' => 'conformarGrupos')); ?> 
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php echo $this->Js->writeBuffer();?>