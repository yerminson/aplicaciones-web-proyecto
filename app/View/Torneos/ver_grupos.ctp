<div class="grid_16"> 
    <div class="block" id="tables">  
        <div class="box" id="verGrupos">
            <h2>
                <a href="#" id="">Ver grupos de un Torneo</a>
            </h2>
            <br/>
            <?php
                echo $this->Form->create('Torneo', array('action' => 'verGrupos'));
                echo $this->Form->input('gran_torneo', array('type' => 'select', 'options' => $nombreTorneos, 'label' => 'Seleccione el Torneo', 'id' => 'gran_torneo'));
                echo $this->Form->end();        
                echo $this->Js->get('#gran_torneo')->event('change', $this->Js->request(array('action' => 'verGrupos'), array(
                        'update' => '#grupos',
                        'method' => 'post',
                        'async' => true,
                        'dataExpression' => true,
                        'data' => $this->Js->serializeForm(array(
                            'isForm' => false,
                            'inline' => true
                        ))
                    )));
            ?>
            <br/>             
            <div id="grupos">
                <?php include 'grupos.ctp';?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Js->writeBuffer();?>