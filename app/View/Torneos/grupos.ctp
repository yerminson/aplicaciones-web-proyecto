<script>
    $(document).ready(function() {
    $("#accordion").accordion();});
</script>

<fieldset>
    <legend>Grupos del Torneo</legend>
    <div id="accordion">
        <?php foreach ($gruposGranTorneo as $gruposTorneo):?>
            <h3 class="toggler atStart ui-accordion-header ui-helper-reset ui-state-active ui-corner-top" role="tab" aria-expanded="true" tabindex="0" style="font-weight: bold; background-color: rgb(255, 255, 255); background-position: initial initial; background-repeat: initial initial; "><span class="ui-icon ui-icon-triangle-1-s"></span>
                <?php echo "Categoria ", $gruposTorneo['torneo']?></h3>
            <div>
                <fieldset>
                    <?php
                        $i = 0; 
                        foreach ($gruposTorneo['grupo'] as $grupo):
                    ?>
                        <h5><?php echo "Grupo " , ++$i; ?></h5>
                        <table>
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Ranking</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($grupo as $jugador):
                                ?>
                                <tr>
                                    <td><?php echo $jugador['Jugador']['nombres'], " ", $jugador['Jugador']['apellidos']; ?>&nbsp;</td>
                                    <td><?php echo $jugador['Jugador']['ranking']; ?>&nbsp;</td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endforeach; ?>
                </fieldset>
            </div>
        <?php endforeach; ?>
    </div>
</fieldset>