<script>
    $(document).ready(function() {
    $("#accordion").accordion();});
</script>

<fieldset>
    <legend>Grupos del Torneo</legend>
    <div id="accordion">
        <?php foreach ($posiciones as $jugadores_por_partidos_ganados):?>
            <h3 class="toggler atStart ui-accordion-header ui-helper-reset ui-state-active ui-corner-top" role="tab" aria-expanded="true" tabindex="0" style="font-weight: bold; background-color: rgb(255, 255, 255); background-position: initial initial; background-repeat: initial initial; "><span class="ui-icon ui-icon-triangle-1-s"></span>
                <?php echo "Categoria ", $jugadores_por_partidos_ganados['torneo']?></h3>
            <div>
                <fieldset>                                           
                    <table>
                        <thead>
                            <tr>
                                <th>Posición</th>
                                <th>Nombre</th>
                                <th>Partidos Ganados</th>
                                <th>Partidos Perdidos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $i = 0; 
                                foreach ($jugadores_por_partidos_ganados['jugadores'] as $jugador):
                            ?> 
                            <tr>
                                <td><?php echo ++$i ?>
                                <td><?php echo $jugador['Jugador']['nombres'], " ", $jugador['Jugador']['apellidos']; ?>&nbsp;</td>
                                <td><?php echo $jugador['Jugador']['partidosGanados']; ?>&nbsp;</td>
                                <td><?php echo $jugador['Jugador']['partidosPerdidos']; ?>&nbsp;</td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>                    
                </fieldset>
            </div>
        <?php endforeach; ?>
    </div>
</fieldset>