<div class="grid_16"> 

    <div class="block" id="tables">  
        <div class="box" id="verCategorias">
                <h2>
                    <a href="#" id="">Consultar categorias del torneo</a>
                </h2>
                <br/>
                <?php if(count($torneos)>0): ?>
                <table>
                <thead>
                    <tr>
                        <th>Categoria</th>
                        <th>Jgadores inscritos</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($torneos as $torneo): ?>
                        <tr>
                            <td><?php echo $torneo['Torneo']['categoria'] ?></td>
                            <td><?php echo count($torneo['Torneo']['jugadores']); ?></td>
                        </tr>
                     <?php endforeach; ?>
                </tbody>
                </table>
                <?php endif; ?>              
        </div>
    </div>
</div>
<?php echo $this->Js->writeBuffer();?>
