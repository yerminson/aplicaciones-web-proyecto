<table>
    <thead>
        <tr>
            <th>Identificación</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($jugadores as $jugador): ?>
            <tr>
                <td><?php echo $jugador['Jugador']['identificacion'] ?></td>
                <td><?php echo $jugador['Jugador']['nombres'] ?></td>
                <td><?php echo $jugador['Jugador']['apellidos'] ?></td>
                <td>
                    <?php echo $this->Html->link('Detalle', array('action' => 'detalle', $jugador['Jugador']['_id'])) ?> &nbsp;
                    <?php echo $this->Html->link('Editar', array('action' => 'editar', $jugador['Jugador']['_id'])) ?>
                    <!--<?php
                echo $this->Form->postLink(
                        'Eliminar', array('action' => 'Eliminar', $jugador['Jugador']['_id']), array('confirm' => 'Realmente está seguro?'))
                    ?>-->
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
