<?php echo $this->Html->script('jquery', FALSE) ?>
<div class="grid_16"> 
    <div class="block" id="tables" >
        <div class="box" id="rankings">
            <h2>
                <a href="#" id="">Ranking</a>
            </h2>
            <br/>

            <?php
            echo $this->Form->create('Jugador');
            echo $this->Form->input('modalidad', array('type' => 'select',
                'options' => array('Masculino' => 'Masculino', 'Femenino' => 'Femenino', 'mixto' => 'Parejas'),
                'label' => 'Opción de Búsqueda', 'id' => 'modalidad'));
            echo $this->Form->end();

            echo $this->Js->get('#modalidad')->event('change', 
                    $this->Js->request(array('controller' => 'jugadors', 'action' => 'rankings'), array(//'update' => '#success',
                        'update' => '#ranking',
                        'method' => 'post',
                        'async' => true,
                        'dataExpression' => true,
                        'data' => $this->Js->serializeForm(array(
                            'isForm' => false,
                            'inline' => true
                        ))
                    )));
            ?>
            <br/><br/>
            <div id="ranking">
                <?php include 'ranking.ctp'; ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Js->writeBuffer(); ?>