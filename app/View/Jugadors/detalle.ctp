<div class="grid_16"> 
    <div class="block" >
        <div class="box" id="users-box">

            <h2>Detalle de datos del Jugador</h2>
            <br/>
            <table>
                <tbody>
                    <tr>
                        <td>Identificación:</td>
                        <td><?php echo $jugador['Jugador']['identificacion'] ?></td>
                    </tr>
                    <tr>
                        <td>Nombres:</td>
                        <td><?php echo $jugador['Jugador']['nombres'] ?></td>
                    </tr>
                    <tr>
                        <td>Apellidos:</td>
                        <td><?php echo $jugador['Jugador']['apellidos'] ?></td>
                    </tr>
                    <tr>
                        <td>Fecha de Nacimiento:</td>
                        <td>Día: <?php echo $jugador['Jugador']['fecha_nacimiento']['day'] ?>
                            Mes: <?php echo $jugador['Jugador']['fecha_nacimiento']['month'] ?>
                            Año: <?php echo $jugador['Jugador']['fecha_nacimiento']['year'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Género:</td>
                        <td><?php echo $jugador['Jugador']['genero'] ?></td>
                    </tr>
                    <tr>
                        <td>Categoria Actual:</td>
                        <td><?php echo $jugador['Jugador']['categoria'] ?></td>
                    </tr>
                    <tr>
                        <td>Ranking:</td>
                        <td><?php echo $jugador['Jugador']['ranking'] ?></td>
                    </tr>
                </tbody>
            </table>
            <?php
            ?>
        </div>
    </div>
</div>

