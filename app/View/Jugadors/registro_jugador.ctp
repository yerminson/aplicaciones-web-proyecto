
<?php

echo $this->Form->hidden('_id', array('value' => $id));
//echo $this->Form->input('identificacion', array('label' => 'Identificación', 'value' => $ide));
echo $this->Form->input('nombres', array('value' => $nombres));
echo $this->Form->input('apellidos', array('value' => $apellidos));
echo $this->Form->input('fecha_nacimiento', array('type' => 'date', 'dateFormat' => 'DMY', 'maxYear' => date('Y') - 18, 'minYear' => date('Y') - 100, 'label' => 'Fecha de Nacimiento', 'selected' => $fecha_nacimiento));
echo $this->Form->input('genero', array('label' => 'Género', 'type' => 'select', 'options' => array('Masculino' => 'Masculino', 'Femenino' => 'Femenino'), 'value' => $genero));
echo $this->Form->input('categoria', array('type' => 'select', 'label' => 'Selecciona la categoría', 'options' => $categorias, 'value' => $categoria));
?>