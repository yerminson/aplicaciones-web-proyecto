<table>
    <thead>
        <tr>
            <th>Ranking</th>
            <th>Nombre</th>
            <th>Puntos</th>
        </tr>
    </thead>
    <tbody>
        <?php $i=0;
              foreach ($jugadores as $jugador): 
              ++$i;
              if($genero == 'mixto'): 
        ?>
                <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $jugador['Pareja']['nombresJ1']," ", $jugador['Pareja']['apellidosJ1']," - ",$jugador['Pareja']['nombresJ2'], " ", $jugador['Pareja']['apellidosJ2']?></td>
                    <td><?php echo $jugador['Pareja']['ranking'] ?></td>                    
                </tr>
            <?php else: ?>
                <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $jugador['Jugador']['nombres']," ",$jugador['Jugador']['apellidos']?></td>
                    <td><?php echo $jugador['Jugador']['ranking'] ?></td>                    
                </tr>                 
            <?php endif; ?>    
        <?php endforeach; ?>
    </tbody>
</table>