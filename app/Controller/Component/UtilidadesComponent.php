<?php
App::import('Model','GranTorneo');
App::import('Model','Torneo');
App::import('Model', 'Jugador');
App::import('Model', 'Pareja');

class UtilidadesComponent extends Component
{
    private $GranTorneo;
    private $Torneo;
    private $Jugador;
    private $Pareja;
    
    function UtilidadesComponent()
    {
        $this->GranTorneo = new GranTorneo();
        $this->Torneo = new Torneo();
        $this->Jugador = new Jugador();
        $this->Pareja = new Pareja();
    }
    /*
     * funcion que retorna las categorias disponibles para un torneo
     * $categoria: indica la categoria actual
     */
    function getCategorias($categoria) {
        $categorias = array();
        if ($categoria == 'Novatos' || $categoria == '') {
            $categorias['1A'] = '1A';
            $categorias['2A'] = '2A';
            $categorias['3A'] = '3A';
            $categorias['4A'] = '4A';
            $categorias['Novatos'] = 'Novatos';
        } else if ($categoria == '4A') {
            $categorias['1A'] = '1A';
            $categorias['2A'] = '2A';
            $categorias['3A'] = '3A';
            $categorias['4A'] = '4A';
        } else if ($categoria == '3A') {
            $categorias['1A'] = '1A';
            $categorias['2A'] = '2A';
            $categorias['3A'] = '3A';
        } else if ($categoria == '2A') {
            $categorias['1A'] = '1A';
            $categorias['2A'] = '2A';
        } else {
            $categorias['1A'] = '1A';
        }

        return $categorias;
    }

    /* función que verfica si el grantorneo aun admite participantes
     * $id: es el identificador del grantorneo
     */

    function torneoAdmiteInscripcion($id) {
        $torneo = $this->GranTorneo->find('all', array('conditions' => array('_id' => $id),
            'fields' => array('cantidad_maxima_jugadores', 'jugadores_inscritos')));
        if ($torneo[0]['GranTorneo']['jugadores_inscritos'] < $torneo[0]['GranTorneo']['cantidad_maxima_jugadores']) {
            return true;
        } else {
            return false;
        }
    }
    
    /*
     * funcion que aumenta en uno la cantidad de jugadores inscritos a un grantorneo
     * $id: es el identificador del grantorneo
     * $val: valor a agregar
     */
    function actualizarCantidadJugadores($id, $val){
        $this->GranTorneo->id = $id;
        $this->GranTorneo->mongoNoSetOperator = '$inc';
        $this->GranTorneo->saveField('jugadores_inscritos',$val);
    }
    
    /*
     * funcion que busca todos los grantorneos existentes y retorna sus nombres en un arreglo
     * $estado: indica el estado del torneo, usado para filtrar el nombre de los torneos
     */
    function getNombresGranTorneo($estado) {        
        $nombresGranTorneos = $this->GranTorneo->find('all', array(
            'conditions' => array('estado' => array('$regex' => new MongoRegex("/$estado/"))),
            'fields' => array('_id', 'nombre')));
        $nombres = array();
        foreach ($nombresGranTorneos as $nombreGranTorneo) {
            $nombres[$nombreGranTorneo['GranTorneo']['_id']] = $nombreGranTorneo['GranTorneo']['nombre'];
        }
        return $nombres;
    }
    
    /*
     * funcion que guarda un torneo, si ya existe agrega el participante, si no existe lo crea
     * $data: contiene los datos del toneo, es un array. Debe contener categoria, modalidad, gran_torneo, y genero si es el caso
     * $id: es el _id del jugador o de la pareja que se desea agregar al torneo
     */

    function guardarTorneo($data, $idParticipante) {
        $jugador = array('_id' => $idParticipante, 'grupo' => 0);

        //se revisa si existe el torneo
        $torneos = $this->Torneo->find('all', array('conditions' => $data));

        if (count($torneos) == 0) {//si el torneo aun no existe se guarda
            $data['jugadores'] = array($jugador);
            $this->Torneo->save($data);
        } else { //si el torneo ya existe agregar el id del participante
            $this->Torneo->mongoNoSetOperator = '$push'; //para que no use $set sino $push
            $this->Torneo->save(array('_id' => $torneos[0]['Torneo']['_id'], 'jugadores' => $jugador));
        }
    }
    
    /*
     * funcion que verfica si un participante ya está registrado en un grantorneo
     * $idGranTroneo: es el identificador de un GranTroneo
     * $IdParticipante: puede ser el identificador de un Jugador o de una Pareja
     */
    function estaRegistrado($idGranTorneo,$idParticipante) {
        $datos = $this->Torneo->find('all', array('conditions' => array('gran_torneo' => $idGranTorneo,
                                                                        'jugadores._id' => $idParticipante)));
        if(count($datos) > 0){
            return true;
        }else{
            return false;
        }
    }
    
    /*
     * funcion que retorna todos los jugadores existentes en un grantorneo
     * $id: es el identificador del gran torneo
     * $opcion: es la modalidad del participante, solo o pareja
     */
    
    function getParticipantesTorneo($id, $opcion){
        
        $torneos = $this->Torneo->find('all',array('conditions' => array('modalidad' => $opcion, 'gran_torneo' => $id), 
                                                       'order' => array('categoria' => 'ASC'),
                                                       'fields' => array('jugadores', 'categoria')
                                                        ));
        $participantes = array();
        if($opcion == 'Solo'){
            foreach ($torneos as $torneo){
                $categoria = $torneo['Torneo']['categoria'];
                foreach ($torneo['Torneo']['jugadores'] as $jugador){
                    $fields = array('identificacion', 'nombres', 'genero', 'categoria');
                    $jug = $this->Jugador->read($fields, $jugador['_id']);
                    $jug['Jugador']['categoria'] = $categoria;
                    array_push($participantes, $jug);
                }
            }
        }else if($opcion == 'Pareja'){
            foreach ($torneos as $torneo){
                $categoria = $torneo['Torneo']['categoria'];
                foreach ($torneo['Torneo']['jugadores'] as $jugador){
                    $fields = array('identificacionJ1','identificacionJ2', 'nombresJ1', 'nombresJ2', 'apellidosJ1', 'apellidosJ2','categoria');
                    $jug = $this->Pareja->read($fields, $jugador['_id']);
                    $jug['Jugador']['categoria'] = $categoria;
                    array_push($participantes, $jug);
                }
            }
        }
        
        //pr($participantes);
        return $participantes;
    }
    
    /*
     * funcion que retorna todos los jugadores existentes en un grantorneo
     * $id: es el identificador del gran torneo
     * $opcion: es la modalidad del participante, solo o pareja
     */
    
    function getParticipantesTorneoPorGenero($id, $genero){
        
        $torneos = $this->Torneo->find('all',array('conditions' => array('genero' => $genero, 'gran_torneo' => $id), 
                                                       'order' => array('categoria' => 'ASC'),
                                                       'fields' => array('jugadores', 'categoria')
                                                        ));
        $participantes = array();
        foreach ($torneos as $torneo){
            $categoria = $torneo['Torneo']['categoria'];
            foreach ($torneo['Torneo']['jugadores'] as $jugador){
                $jug = $this->Jugador->find('first', array(
                    'conditions' => array('_id' =>$jugador['_id'], 'genero'=>$genero),
                    'fields'=> array('identificacion', 'nombres', 'apellidos', 'genero', 'categoria')));
                $jug['Jugador']['categoria'] = $categoria;
                array_push($participantes, $jug);
            }
        }
        return $participantes;
    }
    
    /* funcion que elimina todos los torneos asociasdos a un grantorneo
     * $id: es el identificador del grantorneo
     */
    function eliminarTorneos($id){
        return $this->Torneo->deleteAll(array('gran_torneo' => $id));
    }
    
    
    /*
     * factorial
     * Devuelve el factorial de un número natural
     * @param (int) el número natural
     * @return (long)
     */

    function factorial($natural) {

        // Con esto aseguramos que la función retornará 1 aunque se ingrese 0 como argumento
        $resultado = 1;
        if ($natural > 0) {
            $resultado = $natural * factorial(($natural - 1));
        }

        return $resultado;
    }
    
    /*
     * funcion que calcula la combinatoria de n objetos tomados de m objetos
     */

    function combinatoria($m, $n) {
        $fact = $this->factorial($m)/$this->factorial($n)*$this->factorial($m-$n);
        return $fact;
    }
}
?>
