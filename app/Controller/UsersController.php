<?php

class UsersController extends AppController {

    public $helpers = array('Html', 'Form', 'Js');
    public $name = 'Users';
    public $components = array('Auth'  => array('allowedActions' => array('loginMobile')),'Session', 'Utilidades', 'RequestHandler');

    /**
     * Permite logear usuarios para que accedan al sistema.
     *
     * @return void
     * @access public
     */
    public function login() {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->redirect($this->Auth->redirect());
            } else {    
                $this->Session->setFlash(__('La identificación o la contraseña es incorrecta.'));
            }
        }
    }
    
    public function loginMobile() {
          
        $this->render('login_mobile','ajax');
    }

    /**
     * Permite cerrar sesion a usuarios logeados en el sistema.
     * @return void
     * @access public
     */
    public function logout() {
        $this->redirect($this->Auth->logout());
    }

    function index() {
        
    }

    function registrar() {
        if ($this->request->is('post')) {
            //Se utiliza para que funcione el validador isUnique para el correo.           
            //pr($this->User);
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Ya puedes ingresar al sistema.');
                $this->request->data = array();
                //$this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('No fue posible registrarlo(a) en el sistema, por favor intente de nuevo.');
            }
        }
    }
    
    //funcion asociada con una vista que permite listar todos los usuarios
    function listar(){
        if($this->RequestHandler->isAjax()){
            $valor = $this->request->data['User']['valor'];
            $opcion = $this->request->data['User']['opcion'];
            $this->User->Behaviors->attach('Mongodb.SqlCompatible');
            if($opcion == '1'){//identificacion
                $this->set('users', $this->User->find('all', array('conditions' => array('identificacion LIKE' => '%'.$valor.'%'))));
            }else if($opcion == '2'){//nombres
                $this->set('users', $this->User->find('all', array('conditions' => array('nombres LIKE' => '%'.$valor.'%'))));
            }else if($opcion == '3'){//apellidos
                $this->set('users', $this->User->find('all', array('conditions' => array('apellidos LIKE' => '%'.$valor.'%'))));
            }
            $this->render('lista', 'ajax');
        }else{
            $this->set('users', $this->User->find('all'));
        }
    }
    
    //funciona asociada a una vista que permite mostrar los datos detallados de un usuario
    function detalle($id = null){
        $this->User->id = $id;
        $this->set('usera', $this->User->read());
    }
    
    //funcion asociada a una vista que permite editar los datos un usuario
    function editar($id = null){
        $this->User->id = $id;
         if ($this->request->is('get')) {
            $this->request->data = $this->User->read();
         } else {
            if ($this->User->save($this->request->data)) {
               $this->Session->setFlash('El Usuraio ha sido actualizado.');
               $this->redirect(array('action' => 'listar'));
            }
         }
    }
    
    //funcion que permite eliminar un usuario
    function eliminar($id){
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
         }
         if ($this->User->delete($id)) {
            $this->Session->setFlash('El Usuario ha sido eliminado.');
            $this->redirect(array('action' => 'listar'));
         }
    }

}

?>
