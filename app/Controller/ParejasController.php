<?php

class ParejasController extends AppController {

    public $name = 'Parejas';
    public $helpers = array('Html', 'Form', 'Js');
    public $components = array('Session', 'Utilidades', 'RequestHandler');

    function registrar() {
        if ($this->request->is('post')) {
            if (!empty($this->request->data)) {
                $this->_organizarDatos();

                $datosTorneo = array(
                    'categoria' => $this->request->data['Pareja']['categoria'],
                    'modalidad' => 'Pareja',
                    'gran_torneo' => $this->request->data['Pareja']['gran_torneo'],
                    'genero' => 'mixto'
                );

                //se eliminan los datos que no pertenecen al participante
                unset($this->request->data['Pareja']['gran_torneo']);

                //se agrega el ranking, para parejas nuevas es 0
                $this->request->data['Pareja']['ranking'] = 0;

                if ($this->Utilidades->torneoAdmiteInscripcion($datosTorneo['gran_torneo'])) {

                    $idPareja = $this->request->data['Pareja']['_id'];

                    if (!$this->Utilidades->estaRegistrado($datosTorneo['gran_torneo'], $idPareja)) {
                        if ($idPareja != '') {//la pareja ya existe
                            $this->Pareja->id = $idPareja;
                            if ($this->Pareja->saveField('categoria', $this->request->data['Pareja']['categoria'])) {//se actualiza la categoria de la pareja
                                $this->Utilidades->guardarTorneo($datosTorneo, $idPareja); //se registra la pareja en el torneo
                                $this->Utilidades->actualizarCantidadJugadores($datosTorneo['gran_torneo'],1); //se actualiza la cantidad de jugadores inscritos al grantorneo
                                $this->Session->setFlash('La Pareja ha sido registrada.');
                            } else {
                                $this->Session->setFlash('Ha ocurrido un error, la Pareja no se ha registrado.');
                            }
                        } else {//la pareja aun no existe
                            $this->Pareja->Behaviors->attach('Mongodb.SqlCompatible'); //para que funcione isUnique
                            if ($this->Pareja->save($this->request->data)) { //se guarda la pareja
                                $idPareja = $this->Pareja->find('all', array('conditions' => array('identificacionJ1' => $this->request->data['Pareja']['identificacionJ1'],
                                        'identificacionJ2' => $this->request->data['Pareja']['identificacionJ2']),
                                    'fields' => array('_id'))); //se obtiene el _id de la pareja recien guardada
                                $this->Utilidades->guardarTorneo($datosTorneo, $idPareja[0]['Pareja']['_id']); //se registra la pareja en el torneo
                                $this->Utilidades->actualizarCantidadJugadores($datosTorneo['gran_torneo'],1); //se actualiza la cantidad de jugadores inscritos al grantorneo
                                $this->Session->setFlash('La Pareja ha sido registrada.');
                            } else {
                                $this->Session->setFlash('Ha ocurrido un error, la Pareja no se ha registrado.');
                            }
                        }
                    } else {
                        $this->Session->setFlash('La Pareja actualmente está inscrita en este torneo, NO se puede registrar nuevamente');
                    }
                } else {
                    $this->Session->setFlash('Este torneo no admite más inscripciones.');
                }
            }

            $this->set('nombreTorneos', $this->Utilidades->getNombresGranTorneo('creado'));
            $this->set('categorias', $this->Utilidades->getCategorias(''));
            $this->request->data = array();
        } else {//llamada por get, usado cuando se despliega la vista
            $this->set('nombreTorneos', $this->Utilidades->getNombresGranTorneo('creado'));
            $this->set('categorias', $this->Utilidades->getCategorias(''));
        }
    }

    //arreglar esto
    function _organizarDatos() {
        if (count($this->request->data['Pareja']) == 3) {
            $granTorneo = $this->request->data['Pareja']['gran_torneo'];
            $ide1 = $this->request->data['Pareja']['identificacionJ1'];
            $ide2 = $this->request->data['Pareja']['identificacionJ2'];
            foreach ($this->request->data as $key => $value) {
                $this->request->data['Pareja'][$key] = $value;
                unset($this->request->data[$key]);
            }
            $this->request->data['Pareja']['gran_torneo'] = $granTorneo;
            $this->request->data['Pareja']['identificacionJ1'] = $ide1;
            $this->request->data['Pareja']['identificacionJ2'] = $ide2;
        }
    }

    /*
     * funciona que retorna los datos de la parega, usado por llamado con ajax
     */

    function getDatosParticipante() {
        //pr($this->request->data);
        $id1 = $this->request->data['Pareja']['identificacionJ1'];
        $id2 = $this->request->data['Pareja']['identificacionJ2'];

        $datos = $this->Pareja->find('all', array('conditions' => array('identificacionJ1' => $this->request->data['Pareja']['identificacionJ1'],
                'identificacionJ2' => $this->request->data['Pareja']['identificacionJ2'])));
        if (!empty($datos)) {
            $this->set('id', $datos[0]['Pareja']['_id']);
            $this->set('nombresJ1', $datos[0]['Pareja']['nombresJ1']);
            $this->set('apellidosJ1', $datos[0]['Pareja']['apellidosJ1']);
            $this->set('fecha_nacimientoJ1', $datos[0]['Pareja']['fecha_nacimientoJ1']);
            $this->set('generoJ1', $datos[0]['Pareja']['generoJ1']);
            $this->set('nombresJ2', $datos[0]['Pareja']['nombresJ2']);
            $this->set('apellidosJ2', $datos[0]['Pareja']['apellidosJ2']);
            $this->set('fecha_nacimientoJ2', $datos[0]['Pareja']['fecha_nacimientoJ2']);
            $this->set('generoJ2', $datos[0]['Pareja']['generoJ2']);
            $categoria = $datos[0]['Pareja']['categoria'];
            $this->set('categoria', $categoria);
            $this->set('categorias', $this->Utilidades->getCategorias($categoria));
        } else {
            $this->set('id', '');
            $this->set('nombresJ1', '');
            $this->set('apellidosJ1', '');
            $this->set('fecha_nacimientoJ1', '');
            $this->set('generoJ1', '');
            $this->set('nombresJ2', '');
            $this->set('apellidosJ2', '');
            $this->set('fecha_nacimientoJ2', '');
            $this->set('generoJ2', '');
            $categoria = '';
            $this->set('categoria', $categoria);
            $this->set('categorias', $this->Utilidades->getCategorias($categoria));
        }
        $this->render('registro_pareja', 'ajax');
    }
    
    //funcion asociada con una vista que permite listar todas las parejas
    function listar(){
        if($this->RequestHandler->isAjax()){
            $valor = $this->request->data['Pareja']['valor'];
            $opcion = $this->request->data['Pareja']['opcion'];
            $this->Pareja->Behaviors->attach('Mongodb.SqlCompatible');
            if($opcion == '1'){//identificacion J1
                $this->set('parejas', $this->Pareja->find('all', array('conditions' => array('identificacionJ1 LIKE' => '%'.$valor.'%'))));
            }else if($opcion == '2'){//identificaion J2
                $this->set('parejas', $this->Pareja->find('all', array('conditions' => array('identificacionJ2 LIKE' => '%'.$valor.'%'))));
            }else if($opcion == '3'){//nombres J1
                $this->set('parejas', $this->Pareja->find('all', array('conditions' => array('nombresJ1 LIKE' => '%'.$valor.'%'))));
            }else if($opcion == '4'){
                $this->set('parejas', $this->Pareja->find('all', array('conditions' => array('nombresJ2 LIKE' => '%'.$valor.'%'))));
            }
            $this->render('lista', 'ajax');
        }else{
            $this->set('parejas', $this->Pareja->find('all'));
        }
    }
    
    //funciona asociada a una vista que permite mostrar los datos detallados de una pareja
    function detalle($id = null){
        $this->Pareja->id = $id;
        $this->set('pareja', $this->Pareja->read());
    }
    
    //funcion asociada a una vista que permite editar los datos una pareja
    function editar($id = null){
        $this->Pareja->id = $id;
         if ($this->request->is('get')) {
            $this->request->data = $this->Pareja->read();
         } else {
            if ($this->Pareja->save($this->request->data)) {
               $this->Session->setFlash('La Pareja ha sido actualizada.');
               $this->redirect(array('action' => 'listar'));
            }
         }
    }

}

?>
