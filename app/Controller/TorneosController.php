<?php

class TorneosController extends AppController {

    public $name = 'Torneos';
    public $helpers = array('Html', 'Form', 'Js');
    public $components = array('Session', 'RequestHandler', 'Utilidades');
    public $uses = array('Pareja', 'Jugador', 'Torneo', 'GranTorneo', 'Partido');

    /**
     * Permite conformas los grupos de cada Torneo de un Torneo 
     */
    function conformarGrupos() {
        $todoVaBien = true;
        $borrarTorneo = false;
        $sinTorneos = false;
        /* Buscar los nombres de los torneos que se encuentra en estado creado 
         * y enviarlas a la vista
         */
        $nombresTorneos = $this->Utilidades->getNombresGranTorneo('creado');
        $this->set('nombreTorneos', $nombresTorneos);
        if ($this->request->is('post')) {
            $torneos = $this->Torneo->find('all', array(
                'conditions'=> array('gran_torneo' => $this->request->data['Torneo']['gran_torneo'])
            ));

            if (count($torneos) == 0) {
                $todoVaBien = false;
                $sinTorneos = true;
            }
            foreach ($torneos as $torneo) {
                $jugadores_por_ranking = array();
                $cant_jugadores = 0;
                /*
                 * Por cada torneo se buscan los jugadores inscritos y se guardan
                 * en un array de esta forma: 'ranking' => {jugadores}, para agruparlos
                 * por el ranking y luego ordenarlos de forma ascendente por la clave 
                 * del array, es decir por el ranking.
                 */

                if (count($torneo['Torneo']['jugadores']) < 4) {
                    $id_torneo = $torneo['Torneo']['_id'];
                    $this->Torneo->delete($id_torneo);
                    $borrarTorneo = true;
                    break;
                }

                foreach ($torneo['Torneo']['jugadores'] as $id_jugador) {
                    $jugador = $this->Jugador->find('first', array(
                        'conditions' => array('_id' => $id_jugador['_id']),
                        'fields' => array('ranking')));
                    if (!isset($jugadores_por_ranking[$jugador['Jugador']['ranking']])) {
                        $jugadores_por_ranking[$jugador['Jugador']['ranking']] = array();
                    }
                    array_push($jugadores_por_ranking[$jugador['Jugador']['ranking']], $id_jugador);
                    $cant_jugadores++;
                }
                //Ordena el array de acuerdo a la clave (ranking)
                ksort($jugadores_por_ranking);
                $jugadores = array();
                /*
                 * Ingresa los jugadores en orden al nuevo array que ya nos los agrupa 
                 * por ranking esto para evitar esfuerzos mayores en al llamara al 
                 * método _generarGrupos.
                 */
                foreach ($jugadores_por_ranking as $jpr) {
                    foreach ($jpr as $temp) {
                        array_push($jugadores, $temp);
                    }
                }
                $jugadores_con_grupo = $this->_generarGrupos($cant_jugadores, $jugadores);
                $this->Torneo->id = $torneo['Torneo']['_id'];
                $this->GranTorneo->id = $torneo['Torneo']['gran_torneo'];
                /*
                 * Guardar los jugadores del Torneo con el campo grupo actualizado y cambiar el 
                 * estado del GranTorneo a fase1.
                 */
                if (!$this->Torneo->saveField('jugadores', $jugadores_con_grupo)) {
                    $todoVaBien = false;
                }
            }
            if ($todoVaBien) {
                if ($this->GranTorneo->saveField('estado', 'fase1')) {
                    if($borrarTorneo == true){
                        $this->Session->setFlash('Se borraron algunos torneos ya que no contaban con al menos 4 jugadores.');
                    }
                    $this->redirect(array('action' => 'grupos', $this->GranTorneo->id));
                }
            } else {                
                if ($sinTorneos == true) {
                    $this->Session->setFlash('No hay jugadores inscritos en las categorías para conformar los grupos.');
                }
            }
        } else {
            $this->set('gruposGranTorneo', array());
            if (count($nombresTorneos) == 0) {
                $this->Session->setFlash('No hay torneos disponibles para conformar grupos.');
            }
        }
    }

    /**
     * Permite consultar el estado de inscripción por categorias a cada torneo antes de decidir 
     * conformar los grupos.
     */
    function consultarCategoriasTorneo($idGranTorneo) {
        pr($idGranTorneo);
        $torneos = $this->Torneo->find('all', array(
                'conditions'=> array('gran_torneo' => $idGranTorneo)
            ));
        $this->set('torneos', $torneos);
        if (count($torneos) == 0) {
            $this->Session->setFlash('No hay  jugadores inscritos a las categorias.');
            //$this->redirect(array('action' => 'conformarGrupos'));
        }
    }

    /**
     * Permite asignar el campo grupo a cada jugador de un Torneo 
     * @param type $cant_jugadores
     * @param type $jugadores
     * @return array 
     */
    function _generarGrupos($cant_jugadores, $jugadores) {
        $cant_grupos = ceil($cant_jugadores / 4);
        $residuo = $cant_jugadores % 4;
        $grupos = array();
        if ($residuo != 0) {
            while ($cant_jugadores != 0) {
                if ($residuo != 0) {
                    $jugador = array_pop($jugadores);
                    $jugador['grupo'] = 1;
                    array_push($grupos, $jugador);
                    $residuo--;
                    $cant_jugadores--;
                }
                for ($i = 2; $i <= $cant_grupos; $i++) {
                    $jugador = array_pop($jugadores);
                    $jugador['grupo'] = $i;
                    array_push($grupos, $jugador);
                    $cant_jugadores--;
                }
            }
        } else {
            while ($cant_jugadores != 0) {
                for ($i = 1; $i <= $cant_grupos; $i++) {
                    $jugador = array_pop($jugadores);
                    $jugador['grupo'] = $i;
                    array_push($grupos, $jugador);
                    $cant_jugadores--;
                }
            }
        }
        return $grupos;
    }

    /*
     * Busca los jugadores inscritos en cada Torneo de un GranTorneo
     * y los agrupa por Torneo y grupos en el array $gruposGranTorneo
     */

    function verGrupos() {
        $nombresTorneos = $this->Utilidades->getNombresGranTorneo('fase');
        $this->set('nombreTorneos', $nombresTorneos);
        if ($this->RequestHandler->isAjax()) {
            $this->grupos($this->request->data['Torneo']['gran_torneo']);
            $this->render('grupos', 'ajax');
        } else {
            $this->grupos(key($nombresTorneos));
        }
    }

    //Lista los grupos de un GranTorneo.
    function grupos($idGranTorneo) {
        $torneos = $this->Torneo->find('all', array('conditions' => array('gran_torneo' => $idGranTorneo)));
        $gruposGranTorneo = array();
        foreach ($torneos as $torneo) {
            $jugadores_por_grupo = array();
            $cant_jugadores = 0;
            $genero = $torneo['Torneo']['genero'];
            /*
             * Agrupar los jugadores de acuerdo a su grupo en el array 
             * $jugadores_por_grupo que tiene la forma: grupo=>{jugadores}. 
             */
            foreach ($torneo['Torneo']['jugadores'] as $jugador_torneo) {
                if ($genero != 'mixto') {
                    $jugador = $this->Jugador->find('first', array(
                        'conditions' => array('_id' => $jugador_torneo['_id']),
                        'fields' => array('identificacion', 'nombres', 'apellidos', 'ranking')));
                } else {
                    $pareja = $this->Pareja->find('first', array(
                        'conditions' => array('_id' => $jugador_torneo['_id']),
                        'fields' => array('_id', 'nombresJ1', 'apellidosJ1', 'nombresJ2', 'apellidosJ2', 'ranking')));
                    $jugador = array('Jugador' => array(
                            'identificacion' => $pareja['Pareja']['_id'],
                            'nombres' => $pareja['Pareja']['nombresJ1'] . " " . $pareja['Pareja']['apellidosJ1'] . " -",
                            'apellidos' => $pareja['Pareja']['nombresJ2'] . " " . $pareja['Pareja']['apellidosJ2'],
                            'ranking' => $pareja['Pareja']['ranking']));
                }

                if (!isset($jugadores_por_grupo[$jugador_torneo['grupo']])) {
                    $jugadores_por_grupo[$jugador_torneo['grupo']] = array();
                }
                array_push($jugadores_por_grupo[$jugador_torneo['grupo']], $jugador);
                $cant_jugadores++;
            }
            //Información del Torneo
            $categoria = $torneo['Torneo']['categoria'];
            $modalidad = $torneo['Torneo']['modalidad'];
            /*
             * Guarda en un los grupos en el array $gruposGranTorneo que mantiene 
             * todos los grupos del GranTorneo, agrupados por Torneo.
             */
            $nombreTorneo = "$categoria $modalidad ";
            if ($genero != 'mixto') {
                $nombreTorneo = $nombreTorneo . $genero;
            }
            $gruposTorneo = array('torneo' => $nombreTorneo,
                'grupo' => $jugadores_por_grupo);
            array_push($gruposGranTorneo, $gruposTorneo);
        }
        //Enviar todos los grupos a la vista
        $this->set('gruposGranTorneo', $gruposGranTorneo);
    }

    function verTablaPosiciones() {
        $nombresTorneos = $this->Utilidades->getNombresGranTorneo('fase');
        $this->set('nombreTorneos', $nombresTorneos);
        if ($this->RequestHandler->isAjax()) {
            $this->tablaPosiciones($this->request->data['Torneo']['gran_torneo']);
            $this->render('tablaPosiciones', 'ajax');
        } else {
            $this->tablaPosiciones(key($nombresTorneos));
        }
    }

    function tablaPosiciones($idGranTorneo) {
        $torneos = $this->Torneo->find('all', array('conditions' => array('gran_torneo' => $idGranTorneo)));
        $posiciones = array();
        foreach ($torneos as $torneo) {
            $jugadores_por_partidos_ganados = array();
            $genero = $torneo['Torneo']['genero'];
            /*
             * Agrupar los jugadores de acuerdo a su grupo en el array 
             * $jugadores_por_grupo que tiene la forma: grupo=>{jugadores}. 
             */
            foreach ($torneo['Torneo']['jugadores'] as $jugador_torneo) {
                //Me traigo la informacion de un jugador
                if ($genero != 'mixto') {
                    $jugador = $this->Jugador->find('first', array(
                        'conditions' => array('_id' => $jugador_torneo['_id']),
                        'fields' => array('identificacion', 'nombres', 'apellidos')));
                } else {//Si no es jugador entonces debe ser una pareja
                    $pareja = $this->Pareja->find('first', array(
                        'conditions' => array('_id' => $jugador_torneo['_id']),
                        'fields' => array('_id', 'nombresJ1', 'apellidosJ1', 'nombresJ2', 'apellidosJ2')));
                    $jugador = array('Jugador' => array(
                            'identificacion' => $pareja['Pareja']['_id'],
                            'nombres' => $pareja['Pareja']['nombresJ1'] . " " . $pareja['Pareja']['apellidosJ1'] . " -",
                            'apellidos' => $pareja['Pareja']['nombresJ2'] . " " . $pareja['Pareja']['apellidosJ2']));
                }
                $partidos_jugador = $this->_partidosDeUnJugador($jugador_torneo['_id'], $torneo['Torneo']['_id']);
                $partidos_ganados = 0;
                foreach ($partidos_jugador as $partido) {        
                    if ($partido['Partido']['jugador1']['_id'] == $jugador_torneo['_id']) {
                         
                        if($partido['Partido']['jugador1']['gano'] == true){
                            $partidos_ganados++;
                        }
                    }else{
                         
                        if($partido['Partido']['jugador2']['gano'] == true){
                            $partidos_ganados++;
                        }
                    }
                }
                $jugador['Jugador']['partidosGanados'] = $partidos_ganados;
                $jugador['Jugador']['partidosPerdidos'] = count($partidos_jugador) - $partidos_ganados;
                if (!isset($jugadores_por_partidos_ganados[$partidos_ganados])) {
                    $jugadores_por_partidos_ganados[$partidos_ganados] = array();
                }
                array_push($jugadores_por_partidos_ganados[$partidos_ganados], $jugador);
            }
            //Ordena el array de acuerdo a la clave (ranking)
            krsort($jugadores_por_partidos_ganados);
            $jugadores = array();
            /*
            * Ingresa los jugadores en orden al nuevo array que ya nos los agrupa 
            * por ranking esto para evitar esfuerzos mayores en al llamara al 
            * método _generarGrupos.
            */
            foreach ($jugadores_por_partidos_ganados as $jppg) {
                foreach ($jppg as $temp) {
                    array_push($jugadores, $temp);
                }
            }
            //Información del Torneo
            $categoria = $torneo['Torneo']['categoria'];
            $modalidad = $torneo['Torneo']['modalidad'];
            /*
             * Guarda en un los grupos en el array $gruposGranTorneo que mantiene 
             * todos los grupos del GranTorneo, agrupados por Torneo.
             */
            $nombreTorneo = "$categoria $modalidad ";
            if ($genero != 'mixto') {
                $nombreTorneo = $nombreTorneo . $genero;
            }
            $posicionesTorneo = array('torneo' => $nombreTorneo,
                'jugadores' => $jugadores);
            array_push($posiciones, $posicionesTorneo);
        }
        //Enviar la tabla de posiciones a la vista
        $this->set('posiciones', $posiciones);
    }

    function _partidosDeUnJugador($id_Jugador, $idTorneo) {
        $partidos = array();
        $partidos = $this->Partido->find('all', array('conditions' => array(
                'torneo' => $idTorneo,
                'estado' => 'jugado',
                'jugador1._id' => $id_Jugador
                )));
        if(count($partidos) == 0){
            $partidos = $this->Partido->find('all', array('conditions' => array(
                'torneo' => $idTorneo,
                'estado' => 'jugado',
                'jugador2._id' => $id_Jugador
                )));
        }
        return $partidos;
    }

    /*
     * fucionque elimina un participante de un torneo
     * $idParticipante: es el identificador del partcipante, puede ser jugador o pareja
     * $idGranTorneo: es el identificador del GrantoTorneo
     */

    function quitarDeTorneo($idParticipante, $idGranTorneo) {

        $torneos = $this->Torneo->find('all', array('conditions' => array('gran_torneo' => $idGranTorneo, 'jugadores._id' => $idParticipante)));
        pr($torneos);
        foreach ($torneos as $torneo) {//creo q este for solo se ejecuta una vez
            if (count($torneo['Torneo']['jugadores']) == 1) {//en el torneo solo se encuentra este jugador
                $this->Torneo->id = $torneo['Torneo']['_id'];
                $this->Torneo->delete();
            } else {
                $jugadores = array();
                foreach ($torneo['Torneo']['jugadores'] as $jugador) {
                    if ($jugador['_id'] != $idParticipante) {
                        array_push($jugadores, $jugador);
                    }
                }
                $this->Torneo->id = $torneo['Torneo']['_id'];
                $this->Torneo->saveField('jugadores', $jugadores);
            }
            $this->Utilidades->actualizarCantidadJugadores($idGranTorneo,-1);//resto un jugador
        }

        $this->redirect(array('controller' => 'granTorneos', 'action' => 'editar', $idGranTorneo));
    }

    /*
     * funcion que permite realizar la programación(creacion de partido) de un torneo
     * $idTorneo: es el id del Gran Torneo que se quiere programar
     */

    function realizarProgramacion() {

        $nombresTorneos = $this->Utilidades->getNombresGranTorneo('fase1');
        $this->set('nombreTorneos', $nombresTorneos);
        if ($this->request->is('post')) {

            //se obtiene todos los torneos pertenecientes al gran_torneo del cual se quiere hacer la programacion
            $torneos = $this->Torneo->find('all', array('conditions' => array('gran_torneo' => $this->request->data['Torneo']['gran_torneo']),
                'fields' => array('jugadores')));

            //metemos los jugaodres en un array agrupados por grupo

            foreach ($torneos as $torneo) {
                $cantidadGrupos = 0;
                $jugadoresPorGrupo = array();
                foreach ($torneo['Torneo']['jugadores'] as $jugador) {
                    if (!isset($jugadoresPorGrupo[$jugador['grupo']])) {
                        $jugadoresPorGrupo[$jugador['grupo']] = array();
                    }
                    array_push($jugadoresPorGrupo[$jugador['grupo']], $jugador['_id']);

                    if ($jugador['grupo'] > $cantidadGrupos) {
                        $cantidadGrupos++;
                    }
                }

                //teniendo los jugadores separeados por grupo, se crean los partidos para ese grupo, esto es fase1
                foreach ($jugadoresPorGrupo as $grupo) {
                    $n = count($grupo);
                    if ($n != 1) {
                        for ($i = 0; $i < ($n - 1); $i++) {
                            for ($j = ($i + 1); $j < $n; $j++) {
                                $partido = array('torneo' => $torneo['Torneo']['_id'],
                                    'gran_torneo' => $this->request->data['Torneo']['gran_torneo'],
                                    'cancha' => 0,
                                    'fecha_encuentro' => array(),
                                    'hora_incio' => 0,
                                    'juez' => '',
                                    'jugador1' => array('_id' => $grupo[$i], 'marcador' => 0, 'gano' => false),
                                    'jugador2' => array('_id' => $grupo[$j], 'marcador' => 0, 'gano' => false),
                                    'estado' => 'creado',
                                    'fase' => 'fase1');
                                
                                $this->Partido->id = null;
                                $this->Partido->save($partido);
                            }
                        }
                    }
                }

                //se crean los partidos para las fases > 1
                if ($cantidadGrupos > 1) {
                    $impar = false;
                    if ($cantidadGrupos % 2 != 0) {
                        $impar = true;
                    }
                    $fase = 2;
                    $cantidadPartidos = floor($cantidadGrupos / 2);
                    while ($cantidadPartidos != 1) {
                        //se realiza programacion
                        $this->_generarPartidos($torneo['Torneo']['_id'], $this->request->data['Torneo']['gran_torneo'], 'fase'.$fase, $cantidadPartidos);
                        if($impar){
                            $cantidadPartidos++;
                        }
                        if ($cantidadPartidos % 2 != 0) {
                            $impar = true;
                        }else{
                            $impar = false;
                        }
                        $cantidadPartidos = floor($cantidadPartidos / 2);
                        $fase++;
                    }
                    $this->_generarPartidos($torneo['Torneo']['_id'], $this->request->data['Torneo']['gran_torneo'], 'fase'.$fase, 1);
                }
                $this->GranTorneo->id = $this->request->data['Torneo']['gran_torneo'];
            }
            $this->GranTorneo->saveField('estado', 'faseProgramado');
        }else {
            if (count($nombresTorneos) == 0) {
                $this->Session->setFlash('No hay torneos para ser programados.');
            }
        }
    }
    
    function _generarPartidos($idTorneo, $idGranTorneo, $fase, $cantidad, $idJ1='', $idJ2=''){
        
        for($i=0;$i<$cantidad;$i++){
            $partido = array('torneo' => $idTorneo,
                             'gran_torneo' => $idGranTorneo,
                             'cancha' => 0,
                             'fecha_encuentro' => array(),
                             'hora_incio' => 0,
                             'juez' => '',
                             'jugador1' => array('_id' => $idJ1, 'marcador' => 0, 'gano' => false),
                             'jugador2' => array('_id' => $idJ2, 'marcador' => 0, 'gano' => false),
                             'estado' => 'creado',
                             'fase' => $fase);
            $this->Partido->id = null;
            $this->Partido->save($partido);
        }
    }
}

?>
