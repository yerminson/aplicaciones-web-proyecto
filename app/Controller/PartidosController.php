<?php
class PartidosController  extends AppController{
    
    public $name = 'Partidos';
    public $helpers = array('Html', 'Form', 'Js');
    public $components = array('Session', 'RequestHandler', 'Utilidades');
    public $uses = array('Pareja', 'Jugador', 'Torneo', 'GranTorneo', 'Partido');
    
    function registrarMarcador(){        
        $nombreTorneos = $this->Utilidades->getNombresGranTorneo('fase');
        $this->set('nombreTorneos', $nombreTorneos);
        if($this->RequestHandler->isAjax()){
            $this->auxRegistroMarcadores($this->request->data['Partido']['gran_torneo'], $this->request->data['Partido']['modalidad']);
            $this->render('auxRegistroMarcadores', 'ajax');
        }
        else{
            $this->auxRegistroMarcadores(key($nombreTorneos),'Masculino');
            if($this->request->is('post')){    
                $partido=$this->Partido->find('first', array(
                    'conditions'=> array('gran_torneo'=>$this->request->data['Partido']['gran_torneo'], 
                                         'jugador1._id'=>$this->request->data['Partido']['jugador1'], 
                                         'jugador2._id'=>$this->request->data['Partido']['jugador2'],
                                         'estado'=>'creado')));  
                if(count($partido)==0){
                    $partido=$this->Partido->find('first', array(
                    'conditions'=> array('gran_torneo'=>$this->request->data['Partido']['gran_torneo'], 
                                         'jugador1._id'=>$this->request->data['Partido']['jugador2'], 
                                         'jugador2._id'=>$this->request->data['Partido']['jugador1'],
                                         'estado'=>'creado')));
                    $jugador1=$this->request->data['Partido']['jugador2'];
                    $this->request->data['Partido']['jugador2']=$this->request->data['Partido']['jugador1'];
                    $this->request->data['Partido']['jugador1']=$jugador1;
                }
                if(count($partido) == 0){
                    $this->Session->setFlash('No existen partidos con los dos jugadores ingresados, intente de nuevo.');
                }else{
                    //$this->request->data['Partido']['_id']=$partido['Partido']['_id'];                    
                    $marcador1=$this->request->data['Partido']['marcador1'];
                    $marcador2=$this->request->data['Partido']['marcador2'];
                    $jugador1=$this->request->data['Partido']['jugador1'];
                    $jugador2=$this->request->data['Partido']['jugador2'];
                    unset($this->request->data['Partido']['modalidad']);
                    unset($this->request->data['Partido']['jugador1']);
                    unset($this->request->data['Partido']['jugador2']);
                    unset($this->request->data['Partido']['marcador1']);
                    unset($this->request->data['Partido']['marcador2']); 
                    $this->request->data['Partido']['jugador1']['_id']=$jugador1;
                    $this->request->data['Partido']['jugador2']['_id']=$jugador2;
                    $this->request->data['Partido']['jugador1']['marcador']=$marcador1;
                    $this->request->data['Partido']['jugador2']['marcador']=$marcador2;
                    if($marcador1>$marcador2){
                        $this->request->data['Partido']['jugador1']['gano']=true;
                        $this->request->data['Partido']['jugador2']['gano']=false;
                    }else{
                        $this->request->data['Partido']['jugador2']['gano']=true;
                        $this->request->data['Partido']['jugador1']['gano']=false;
                    }          
                    $this->request->data['Partido']['estado']='jugado';            
                    $this->Partido->id = $partido['Partido']['_id'];                    
                    if($this->Partido->save($this->request->data) /*and $this->Partido->saveField('estado', 'jugado')*/){
                        $this->Session->setFlash('EL resultado del partido se registró exitosamente.');
                        $this->request->data=array();
                    }
                }
            }
        }
        
    }
      
        
    function auxRegistroMarcadores($idGranTorneo, $modalidad){
        $jugadores=array();
        if($modalidad == 'Parejas'){
            $parejas  = $this->Utilidades->getParticipantesTorneo($idGranTorneo, 'Pareja');
            foreach($parejas as $pareja){
                $jugadores[$pareja['Pareja']['_id']] = $pareja['Pareja']['nombresJ1'].' '.$pareja['Pareja']['apellidosJ1'].' - '.$pareja['Pareja']['nombresJ2'].' '.$pareja['Pareja']['apellidosJ2'];
            }
        }else{            
            $jugadores_temp  = $this->Utilidades->getParticipantesTorneoPorGenero($idGranTorneo, $modalidad);
            foreach($jugadores_temp as $jugador){
                $jugadores[$jugador['Jugador']['_id']] = $jugador['Jugador']['nombres'].' '.$jugador['Jugador']['apellidos'];
            }
        }        
        //pr($jugadores);
        $this->set('jugadores', $jugadores);
    }   
    
    
    function grupos($idGranTorneo){
        $torneos = $this->Torneo->find('all', 
                array('conditions' => array('gran_torneo' => $idGranTorneo)));
        $gruposGranTorneo = array();
        foreach($torneos as $torneo){
            $jugadores_por_grupo=array();
            $cant_jugadores = 0;            
            $genero = $torneo['Torneo']['genero'];
            /*
            * Agrupar los jugadores de acuerdo a su grupo en el array 
            * $jugadores_por_grupo que tiene la forma: grupo=>{jugadores}. 
            */
            foreach ($torneo['Torneo']['jugadores'] as $jugador_torneo){
                if($genero != 'mixto'){
                    $jugador = $this->Jugador->find('first', array(
                        'conditions' => array('_id' =>$jugador_torneo['_id']),
                        'fields' => array('identificacion', 'nombres', 'apellidos', 'ranking')));
                }else{
                    $pareja = $this->Pareja->find('first', array(
                        'conditions' => array('_id' =>$jugador_torneo['_id']),
                        'fields' => array('_id', 'nombresJ1', 'apellidosJ1','nombresJ2', 'apellidosJ2', 'ranking')));
                    $jugador = array ('Jugador' => array(
                        'identificacion' => $pareja['Pareja']['_id'],
                        'nombres' => $pareja['Pareja']['nombresJ1']." ".$pareja['Pareja']['apellidosJ1']." -", 
                        'apellidos' => $pareja['Pareja']['nombresJ2']." ".$pareja['Pareja']['apellidosJ2'],
                        'ranking'=> $pareja['Pareja']['ranking']));
                }

                if(!isset($jugadores_por_grupo[$jugador_torneo['grupo']])){
                    $jugadores_por_grupo[$jugador_torneo['grupo']] = array();
                }
                array_push($jugadores_por_grupo[$jugador_torneo['grupo']], $jugador);
                $cant_jugadores++;                
            }
            //Información del Torneo
            $categoria = $torneo['Torneo']['categoria'];
            $modalidad = $torneo['Torneo']['modalidad'];
            /*
            * Guarda en un los grupos en el array $gruposGranTorneo que mantiene 
            * todos los grupos del GranTorneo, agrupados por Torneo.
            */
            $nombreTorneo = "$categoria $modalidad ";
            if($genero != 'mixto'){
                $nombreTorneo = $nombreTorneo.$genero;
            }
            $gruposTorneo = array('torneo' => $nombreTorneo,
                'grupo' => $jugadores_por_grupo);
            array_push($gruposGranTorneo, $gruposTorneo);
        }
        //Enviar todos los grupos a la vista
        $this->set('gruposGranTorneo', $gruposGranTorneo);
    }
    
    function listar(){
        if($this->RequestHandler->isAjax()){
            $this->set('partidos', $this->Partido->find('all', array('conditions' => array('gran_torneo' => $this->request->data['Partido']['gran_torneo']))));
        }else{
            $nombres = $this->Utilidades->getNombresGranTorneo('faseProgramado');
            $this->set('nombresTorneos', $nombres);
            //pr($nombres);
            $this->set('partidos', $this->Partido->find('all', array('conditions' => array('gran_torneo' => key($nombres)))));
        }
    }
    
}
?>