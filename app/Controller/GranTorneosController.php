<?php

class GranTorneosController extends AppController {

    public $helpers = array('Html', 'Form');
    public $name = 'GranTorneos';
    public $components = array('Session', 'Utilidades');

    public function registrar() {
        if ($this->request->is('post')) {
            //pr($this->request->data);
            $this->_convertirNumero('guardar');
            //se agrega un campo adicional para llevar la cuenta de cuantos jugadores se han inscrito al torneo
            $this->request->data['GranTorneo']['jugadores_inscritos'] = 0;
            //se agrega otro campo adicional para llevar el control del estado del torneo
            $this->request->data['GranTorneo']['estado'] = 'creado';

            $this->GranTorneo->Behaviors->attach('Mongodb.SqlCompatible'); //para que funcione isUnique
            if ($this->GranTorneo->save($this->request->data)) { //se guarda el jugador
                $this->Session->setFlash('El torneo a sido registrado.');
                $this->request->data = array();
                //$this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('A ocurrido un error, el torneo no se a podido registrar');
            }
        }
    }

    //funcion que convierte a numero los campos necesarios
    // $opcion: pusde ser guardar o editar
    function _convertirNumero($opcion) {
        if ($opcion == 'guardar') {
            $duracion = $this->request->data['GranTorneo']['duracion'];
            if ($duracion != '') {
                $this->request->data['GranTorneo']['duracion'] = intval($duracion);
            }
            $costo = $this->request->data['GranTorneo']['costo_inscripcion'];
            if ($costo != '') {
                $this->request->data['GranTorneo']['costo_inscripcion'] = intval($costo);
            }
            $canchas = $this->request->data['GranTorneo']['canchas_disponibles'];
            if ($canchas != '') {
                $this->request->data['GranTorneo']['canchas_disponibles'] = intval($canchas);
            }
            $jugadores = $this->request->data['GranTorneo']['cantidad_maxima_jugadores'];
            if ($jugadores != '') {
                $this->request->data['GranTorneo']['cantidad_maxima_jugadores'] = intval($jugadores);
            }
        }
        for ($i = 0; $i < count($this->request->data['GranTorneo']['premios']); $i++) {
            $valor = $this->request->data['GranTorneo']['premios'][$i]['valor'];
            //if($valor != '')
            $this->request->data['GranTorneo']['premios'][$i]['valor'] = intval($valor);
        }
    }

    function listar() {
        $torneos = $this->GranTorneo->find('all');
        $this->set('torneos', $torneos);
    }

    function detalle($id = null) {
        $this->GranTorneo->id = $id;
        $this->set('torneo', $this->GranTorneo->read());
        $this->set('jugadores', $this->Utilidades->getParticipantesTorneo($id, 'Solo'));
        $this->set('parejas', $this->Utilidades->getParticipantesTorneo($id, 'Pareja'));
        $this->Utilidades->getParticipantesTorneo($id, 'Solo');
    }

    function editar($id = null) {
        $this->GranTorneo->id = $id;
        if ($this->request->is('get')) {
            $this->request->data = $this->GranTorneo->read();
            $this->set('jugadores', $this->Utilidades->getParticipantesTorneo($id, 'Solo'));
            $this->set('parejas', $this->Utilidades->getParticipantesTorneo($id, 'Pareja'));
        } else {
            $this->_convertirNumero('editar');
            if ($this->GranTorneo->save($this->request->data)) {
                $this->Session->setFlash('El Torneo ha sido actualizado.');
                $this->redirect(array('action' => 'listar'));
            }
        }
    }

    function eliminar($id) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
         }
         if ($this->GranTorneo->delete($id)) {
            if($this->Utilidades->eliminarTorneos($id)){
                $this->Session->setFlash('El torneo ha sido eliminado.');
                $this->redirect(array('action' => 'listar'));
            } else{
                $this->Session->setFlash('Ha ocurrido un error. El Gran Torneo ha sido eliminado pero los torneos asociados NO han sido eliminados.');
            }
         } else {
             $this->Session->setFlash('Ha ocurrido un error. El torneo NO ha sido eliminado.');
         }
    }

}

?>
